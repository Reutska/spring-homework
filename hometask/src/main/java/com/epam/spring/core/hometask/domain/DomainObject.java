package com.epam.spring.core.hometask.domain;

import java.util.concurrent.atomic.AtomicLong;

public class DomainObject {

	private static final AtomicLong count = new AtomicLong(0);
	private Long id;
	
	public Long getId() {
		return id;
		
	}

	public DomainObject() {
		id=count.incrementAndGet();
	}
	
}
