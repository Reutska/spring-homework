package com.epam.spring.core.hometask.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.epam.spring.core.hometask.domain.User;

public class UserSevice implements AbstractService<User> {
	private List<User> users = new ArrayList();

	public User save(User user) {
		users.add(user);
		return user;
	}

	public void remove(User user) {
		users.remove(user);

	}

	public User getById(Long id) {

		return users.stream().filter(user -> user.getId() == id).findAny().orElse(null);

	}

	public User getUserByEmail(String email) {

		return users.stream().filter(user -> email.equals(user.getEmail())).findAny().orElse(null);
	}

	public Collection<User> getAll() {
		return users;
	}

}
