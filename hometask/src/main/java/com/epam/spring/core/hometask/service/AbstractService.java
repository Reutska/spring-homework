package com.epam.spring.core.hometask.service;

import java.util.Collection;

import com.epam.spring.core.hometask.domain.DomainObject;

public interface AbstractService <T extends DomainObject>{
	
	public  T save(T object);
	
	public void  remove(T object);
	
	public T getById(Long id);
	
	
	public Collection<T> getAll();
	
	

}
