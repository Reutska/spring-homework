package com.epam.spring.core.hometask.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.epam.spring.core.hometask.domain.Event;

public class EventService implements AbstractService<Event> {

	private List<Event> events = new ArrayList<>();

	@Override
	public Event save(Event event) {
		events.add(event);
		return event;
	}

	@Override
	public void remove(Event event) {
		events.remove(event);

	}

	@Override
	public Event getById(Long id) {
		return events.stream().filter(event -> event.getId() == id).findAny().orElse(null);
	}

	public Event getEventByName(String name) {
		return events.stream().filter(event -> name.equals(event.getName())).findAny().orElse(null);
	}

	@Override
	public Collection<Event> getAll() {
		return events;
	}

}
