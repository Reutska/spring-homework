package com.epam.spring.core.hometask.domain.discount;

public interface DiscountStrategy {
    public byte calculateDiscount();
}
