package com.epam.spring.core.hometask.domain;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Event extends DomainObject {

	private String name;

	private Set<LocalDateTime> airDates = new TreeSet<>();

	private double basePrice;

	private EventRating rating;

	private Map<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<LocalDateTime> getAirDates() {
		return airDates;
	}

	public void setAirDates(Set<LocalDateTime> airDates) {
		this.airDates = airDates;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public EventRating getRating() {
		return rating;
	}

	public void setRating(EventRating rating) {
		this.rating = rating;
	}

	public Map<LocalDateTime, Auditorium> getAuditoriums() {
		return auditoriums;
	}

	public void setAuditoriums(Map<LocalDateTime, Auditorium> auditoriums) {
		this.auditoriums = auditoriums;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airDates == null) ? 0 : airDates.hashCode());
		result = prime * result + ((auditoriums == null) ? 0 : auditoriums.hashCode());
		long temp;
		temp = Double.doubleToLongBits(basePrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (airDates == null) {
			if (other.airDates != null)
				return false;
		} else if (!airDates.equals(other.airDates))
			return false;
		if (auditoriums == null) {
			if (other.auditoriums != null)
				return false;
		} else if (!auditoriums.equals(other.auditoriums))
			return false;
		if (Double.doubleToLongBits(basePrice) != Double.doubleToLongBits(other.basePrice))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rating != other.rating)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Event [name=" + name + ", airDates=" + airDates + ", basePrice=" + basePrice + ", rating=" + rating
				+ ", auditoriums=" + auditoriums + "]";
	}

}
