package com.epam.spring.core.hometask.service;

import java.util.Set;
import java.util.TreeSet;

import com.epam.spring.core.hometask.domain.Auditorium;

public class AuditoriumService {

	private Set<Auditorium> auditoriums = new TreeSet<>();

	public Set<Auditorium> getAll() {
		return auditoriums;
	}

	public Auditorium getByName(String name) {
		return auditoriums.stream().filter(auditorium -> name.equals(auditorium.getName())).findAny().orElse(null);
	}

}
