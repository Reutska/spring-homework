package com.epam.spring.core.hometask.domain.discount;

public class BoughtTicketsCountStrategy implements DiscountStrategy {
    @Override
    public byte calculateDiscount() {
        return 0;
    }
}
